# fFormGenerator a formgenerator for flourish v. 0.4a#

#### Function ####
This extension enable every user to add an auto-generated form to flourish projects using the main class fForm.
At this time during the development the extending files are located under the plugins folder and configured in the
config.php file.

#### TODO's ####
* extending fFormCheck
* adding fFormRadio
* extending fFormSelect with default selected if is array()
* adding fFormUpload extending fFormInput
* adding fFormFieldSet

#### Examples ####
* You will find some examples in the start.php file located in the root folder.


#### Chages ####
* v. 0.4 -- adding base functionality to fFormCheck
* v. 0.3 -- adding fFormDecorator
* v. 0.3 -- adding fFormLabel
* v. 0.3 -- adding basic installation instructions
* v. 0.3 -- implemented CSRF via hidden fFormInput (including fix value)
* v. 0.3 -- redefining some requirements
* v. 0.3 -- added fFormSelect with multiple option
* v. 0.2 -- added fFormTextarea
* v. 0.1 -- added base functionality simple form with inputs.
* v. 0.1 -- added description & todo's

#### Installation ####
* to checkout the project use SourceTree
* you need the following folder: *root/storage/session*

#### Contribution ####
* fork this project
* only pull-request to development branch are legit and will accepted