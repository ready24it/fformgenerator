<?php
/**
 * Represents an html form radio button.
 *
 * @copyright  Copyright (c) 2013 Giacomo Barbalinardo
 * @author     Giacomo Barbalinardo [gb] <info@ready24it.eu>
 * @license    http://flourishlib.com/license
 *
 * @package    Flourish
 * @link       http://flourishlib.com/fFormRadio
 *
 * @version    0.1
 *
 */


//Im Gegensatz zu Radiobuttons müssen Checkboxen keine identische Namen haben, um zu funktionieren. Es ist aber hinsichtlich des auswertenden Skripts eventuell sehr sinnvoll, identische Namen zu verwenden, weil das Skript dann automatisch ein Array anlegt.
//
//Für PHP-Nutzer der Hinweis: Das automatische Anlegen eines Arrays funktioniert nur, wenn der Name des Feldes mit eckigen Klammern endet: name="zutaten[]".

class fFormRadio extends fFormElement {

    function __toString()
    {
        // TODO: Implement __toString() method.
        return '';
    }
}