<?php
/**
 * Represents an html form checkbox field.
 *
 * @copyright  Copyright (c) 2013 Giacomo Barbalinardo
 * @author     Giacomo Barbalinardo [gb] <info@ready24it.eu>
 * @license    http://flourishlib.com/license
 *
 * @package    Flourish
 * @link       http://flourishlib.com/fFormCheck
 *
 * @version    0.1
 *
 */

class fFormCheck extends fFormElement {

    public function __construct(array $config) {
        if(!isset($config['values'])) {
            throw new Exception('Required values for check not supplied');
        }

        parent::__construct($config);
    }

    public function isSelected($key){
        if(is_array($this->getValue())) {
            return in_array($key, $this->getValue());
        }
        return $key == $this->getValue();
    }

    /**
     * @todo implement post and get for automatic checking values after form submit
     */
    public function getValue(){
        return parent::getValue();
    }

    public function __toString()
    {
        $ret = '';
        foreach($this->getValues() as $key => $value) {
            $ret .= '<input type="checkbox" name="' . $this->getName() . '" value="' . $key . '" ';
            $ret .= $this->isSelected($key) ? 'checked' : '';
            $ret .= $this->attributesToString();
            $ret .= '/>';
            $ret .= '' != $value ? '<span>' . $value . '</span>' : '';
        }
        return $ret;
    }
} 