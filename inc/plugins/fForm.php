<?php
/**
 * Represents an html form.
 * 
 * @copyright  Copyright (c) 2013 Giacomo Barbalinardo
 * @author     Giacomo Barbalinardo [gb] <info@ready24it.eu>
 * @license    http://flourishlib.com/license
 * 
 * @package    Flourish
 * @link       http://flourishlib.com/fForm
 * 
 * @version    0.1
 * 
 */
class fForm
{
    protected $_elements = array();
    protected $_method = 'GET';
    protected $_action;
    protected $_cssClass;
    protected $_decorator;

    public function __construct(array $config = array())
    {
        if(!empty($config)){
            $this->setMethod(isset($config['method']) ? $config['method'] : 'GET');

            if (isset($config['action'])) {
                $this->setAction($config['action']);
            }

            $this->setCssClass(isset($config['cssClass']) ? $config['cssClass'] : '');
        }
    }

    public function setMethod($method)
    {
        $this->_method = $method;
        return $this;
    }

    public function setAction($url)
    {
        $this->_action = $url;
        return $this;
    }

    public function setCssClass($cssClass = '')
    {
        $this->_cssClass = $cssClass;
        return $this;
    }

    public function setDecorator(array $config = array())
    {
        $this->_decorator = new fHtmlDecorator($config);
        return $this;
    }

    public function addElement(fFormElement $element)
    {
        if (isset($this->_elements[$element->getName()])) {
            $name = $element->getName();
            $oldValue = $this->_elements[$element->getName()];
            if (!is_array($oldValue)) {
                $element = array($oldValue, $element);
            } else {
                array_push($oldValue, $element);
                $element = $oldValue;
            }
            $this->_elements[$name] = $element;
        } else {
            $this->_elements[$element->getName()] = $element;
        }
        return $this;
    }

    public function addElements(array $elements)
    {
        $this->_elements = array_merge($this->_elements, $elements);
        return $this;
    }

    public function has($property){
        return !empty($this->$property);
    }

    public function getDecorator()
    {
        return $this->_decorator;
    }

    public function getElements()
    {
        return $this->_elements;
    }

    public function __toString()
    {
        $ret  = '<form method="' . $this->_method . '" action="' . $this->_action . '"';
        $ret .= $this->has('_cssClass') ? ' class="' . $this->_cssClass . '"' : '';
        $ret .= '>' . PHP_EOL;
        foreach ($this->getElements() as $elementName => $element) {
            if (!is_array($element)) {
                $element->setMethod($this->_method);
                $ret .= $element->__toString();
            } else {
                foreach ($element as $elem) {
                    $elem->setMethod($this->_method);
                    $ret .= $elem->__toString();
                }
            }
        }
        $ret .= '</form>' . PHP_EOL;
        return $ret;
    }

}