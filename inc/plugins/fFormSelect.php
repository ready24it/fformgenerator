<?php
/**
 * Represents an html form select field.
 *
 * @copyright  Copyright (c) 2013 Giacomo Barbalinardo
 * @author     Giacomo Barbalinardo [gb] <info@ready24it.eu>
 * @license    http://flourishlib.com/license
 *
 * @package    Flourish
 * @link       http://flourishlib.com/fFormInput
 *
 * @version    0.1
 *
 */

class fFormSelect extends fFormElement
{
    protected $_size = null;
    protected $_multiple = false;

    public function __construct(array $config)
    {
        if(!isset($config['values'])) {
            throw new Exception('Required values for select not supplied');
        }
        $this->setSize(isset($config['size']) ? (int) $config['size'] : '');
        $this->setIsMultiple(isset($config['multiple']) ? true : false);
        parent::__construct($config);
    }

    public function setSize($size)
    {
        $this->_size = $size;
        return $this;
    }

    public function getSize()
    {
        return $this->_size;
    }

    public function setIsMultiple($multiple = true)
    {
        $this->_multiple = (bool) $multiple;
        return $this;
    }

    public function isMultiple()
    {
        return $this->_multiple;
    }

    public function getValue()
    {
        if((($this->getMethod() == 'POST' && fRequest::isPost()) || ($this->getMethod() == 'GET' && fRequest::isGet())) && fRequest::get($this->getName(), 'string', false)) {
            return fHtml::encode(fRequest::get($this->getName()));
        } else {
            return $this->_value;
        }
    }

    function __toString()
    {
        $ret  = '<select ';
        $ret .= 'name="' . $this->getName() . '"';
        $ret .= $this->has('_cssClass') ? ' class="' . $this->_cssClass . '"' : '';
        $ret .= $this->has('_cssId') ? ' id="' . $this->_cssId . '"' : '';
        $ret .= $this->has('_size') ? ' size="' . $this->getSize() . '"' : '';
        $ret .= $this->isMultiple() ? ' multiple' : '';
        $ret .= '>' . PHP_EOL;
        if(count($this->getValues())) {
            foreach ($this->getValues() as $value => $name) {
                $ret .= "\t" . '<option value="' . $value . '"';
                $ret .= $value == $this->getValue() ? ' selected' : '';
                $ret .= '>' . $name . '</option>' . PHP_EOL;
            }
        }
        $ret .= '</select>' . PHP_EOL;
        return $ret;
    }
}