<?php
/**
 * Represents an html form input field.
 * 
 * @copyright  Copyright (c) 2013 Giacomo Barbalinardo
 * @author     Giacomo Barbalinardo [gb] <info@ready24it.eu>
 * @license    http://flourishlib.com/license
 * 
 * @package    Flourish
 * @link       http://flourishlib.com/fFormInput
 * 
 * @version    0.3
 * 
 */

class fFormInput extends fFormElement
{
    protected $_fixed = false;

    public function __construct(array $config) {
        if(!isset($config['type'])) {
            throw new Exception('Required type for input not supplied');
        }

        $this->setIsFixed(isset($config['fixed']) ? (bool) $config['fixed'] : false);
        parent::__construct($config);
    }

    public function setIsFixed($fixed = true)
    {
        $this->_fixed = (bool) $fixed;
        return $this;
    }

    public function isFixed()
    {
        return $this->_fixed;
    }

    public function getValue()
    {
        if($this->isFixed()) {
            return $this->_value;
        }

        if((($this->getMethod() == 'POST' && fRequest::isPost()) || ($this->getMethod() == 'GET' && fRequest::isGet())) && fRequest::get($this->getName(), 'string', false)) {
            return fHtml::encode(fRequest::get($this->getName()));
        } else {
            return $this->_value;
        }
    }

    public function __toString() {
        $ret  = '<input ';
        $ret .= 'type="' . $this->getType() . '" ';
        $ret .= 'name="' . $this->getName() . '"';
        $ret .= $this->has('_cssClass') ? ' class="' . $this->_cssClass . '"' : '';
        $ret .= ' id="' . ($this->has('_cssId') ?  $this->_cssId : $this->getName()) . '"';
        $ret .= ' value="' . $this->getValue() . '"';
        $ret .= ' />' . PHP_EOL;
        return $ret;
    }
}