<?php
/**
 * Represents an html form element.
 * 
 * @copyright  Copyright (c) 2013 Giacomo Barbalinardo
 * @author     Giacomo Barbalinardo [gb] <info@ready24it.eu>
 * @license    http://flourishlib.com/license
 * 
 * @package    Flourish
 * @link       http://flourishlib.com/fFormElement
 * 
 * @version    0.2
 * 
 */

abstract class fFormElement
{
    protected $_type;
    protected $_name;
    protected $_cssClass;
    protected $_cssId;
    protected $_method;
    protected $_value;
    protected $_values;
    protected $_attributes = array();

	public function __construct(array $config)
	{
        if(!isset($config['name'])) {
            throw new Exception('Required name for ' . get_class($this) . ' not supplied');
        }

        $this->setCssClass(isset($config['cssClass']) ? $config['cssClass'] : '');
        $this->setCssId(isset($config['cssId']) ? $config['cssId'] : '');
        $this->setName(isset($config['name']) ? $config['name'] : '');
        $this->setType(isset($config['type']) ? $config['type'] : '');
        $this->setValue(isset($config['value']) ? $config['value'] : '');
        $this->setValues(isset($config['values']) ? $config['values'] : array());
        $this->setAttributes(isset($config['attributes']) ? $config['attributes'] : array());
	}

    public function setName($name)
    {
        $this->_name = $name;
        return $this;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function has($property){
        return !empty($this->$property);
    }

    public function setCssClass($cssClass = '')
    {
        $this->_cssClass = $cssClass;
        return $this;
    }

    public function setCssId($cssId = '')
    {
        $this->_cssId = $cssId;
        return $this;
    }

    public function setType($type)
    {
        $this->_type = $type;
        return $this;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function setValue($value)
    {
        $this->_value = $value;
        return $this;
    }

    public function getValue()
    {
        return $this->_value;
    }

    public function setValues(array $values)
    {
        $this->_values = $values;
        return $this;
    }

    public function getValues()
    {
        return $this->_values;
    }

    public function setMethod($method) {
        $this->_method = $method;
        return $this;
    }

    public function getMethod()
    {
        return $this->_method;
    }

    public function setAttributes(array $attributes)
    {
        $this->_attributes = $attributes;
        return $this;
    }

    public function addAttribute($name, $value){
        $this->_attributes[$name] = $value;
        return $this;
    }

    public function getAttributes(){
        return $this->_attributes;
    }

    public function attributesToString(){
        $ret = '';
        foreach ($this->_attributes as $tag => $value) {
            $ret .= ' ' . $tag . '="' . $value . '"';
        }
        return $ret;
    }

    abstract function __toString();
}