<?php
/**
 * Represents an html form element decorator.
 *
 * @copyright  Copyright (c) 2013 Giacomo Barbalinardo
 * @author     Giacomo Barbalinardo [gb] <info@ready24it.eu>
 * @license    http://flourishlib.com/license
 *
 * @package    Flourish
 * @link       http://flourishlib.com/fFormDecorator
 *
 * @version    0.3
 *
 */

class fFormDecorator extends fFormElement
{
    protected $_elements = array();
    protected $_tag = null;

    public function __construct(array $config){
        if(!isset($config['tag'])) {
            throw new Exception('Required tag for decorator not supplied');
        }
        $this->setHtmlTag($config['tag']);
        $this->setCssClass(isset($config['cssClass']) ? $config['cssClass'] : '');
        $this->setCssId(isset($config['cssId']) ? $config['cssId'] : '');
    }

    public function setHtmlTag($tag){
        $this->_tag = $tag;
        return $this;
    }

    public function getHtmlTag(){
        return $this->_tag;
    }

    public function addElement(fFormElement $element)
    {
        if (isset($this->_elements[$element->getName()])) {
            $name = $element->getName();
            $oldValue = $this->_elements[$element->getName()];
            if (!is_array($oldValue)) {
                $element = array($oldValue, $element);
            } else {
                array_push($oldValue, $element);
                $element = $oldValue;
            }
            $this->_elements[$name] = $element;
        } else {
            $this->_elements[$element->getName()] = $element;
        }
        return $this;
    }

    public function addElements(array $elements)
    {
        $this->_elements = array_merge($this->_elements, $elements);
        return $this;
    }

    public function getElements()
    {
        return $this->_elements;
    }

    public function __toString(){
        $ret  = '<' . $this->getHtmlTag() . ' ';
        $ret .= $this->has('_cssClass') ? ' class="' . $this->_cssClass . '"' : '';
        $ret .= $this->has('_cssId') ? ' id="' . $this->_cssId . '"' : '';
        $ret .= '>' . PHP_EOL;
        foreach ($this->getElements() as $element) {
            if (!is_array($element)) {
                $element->setMethod($this->_method);
                $ret .= $element->__toString();
            } else {
                foreach ($element as $elem) {
                    $elem->setMethod($this->_method);
                    $ret .= $elem->__toString();
                }
            }
        }
        $ret .= '</' . $this->getHtmlTag() . '>' . PHP_EOL;
        return $ret;
    }
} 