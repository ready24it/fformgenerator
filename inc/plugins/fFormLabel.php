<?php
/**
 * Represents an html form label element.
 *
 * @copyright  Copyright (c) 2013 Giacomo Barbalinardo
 * @author     Giacomo Barbalinardo [gb] <info@ready24it.eu>
 * @license    http://flourishlib.com/license
 *
 * @package    Flourish
 * @link       http://flourishlib.com/fFormLabel
 *
 * @version    0.2
 *
 */

class fFormLabel extends fFormElement{

    function __construct(array $config)
    {
        if(!isset($config['value'])) {
            throw new Exception('Required value for label not supplied');
        }

        parent::__construct($config);
    }

    function __toString()
    {
        $ret  = '<label ';
        $ret .= 'for="' . $this->getName() . '"';
        $ret .= $this->has('_cssClass') ? ' class="' . $this->_cssClass . '"' : '';
        $ret .= $this->has('_cssId') ? ' id="' . $this->_cssId . '"' : '';
        $ret .= '>' . $this->getValue() . '</label>' . PHP_EOL;
        return $ret;
    }
}