<?php
/**
 * Represents an html form textarea.
 *
 * @copyright  Copyright (c) 2013 Giacomo Barbalinardo
 * @author     Giacomo Barbalinardo [gb] <info@ready24it.eu>
 * @license    http://flourishlib.com/license
 *
 * @package    Flourish
 * @link       http://flourishlib.com/fFormElement
 *
 * @version    0.1
 *
 */

class fFormTextarea extends fFormElement {

    public function getValue()
    {
        if((($this->getMethod() == 'POST' && fRequest::isPost()) || ($this->getMethod() == 'GET' && fRequest::isGet())) && fRequest::get($this->getName(), 'string', false)) {
            return fHtml::encode(fRequest::get($this->getName()));
        } else {
            return $this->_value;
        }
    }

    public function __toString()
    {
        $ret  = '<textarea ';
        $ret .= 'name="' . $this->getName() . '"';
        $ret .= $this->has('_cssClass') ? ' class="' . $this->_cssClass . '"' : '';
        $ret .= ' id="' . ($this->has('_cssId') ?  $this->_cssId : $this->getName()) . '"';
        $ret .= '>';
        $ret .= $this->getValue();
        $ret .= '</textarea>' . PHP_EOL;
        return $ret;
    }
} 