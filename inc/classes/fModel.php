<?php

/**
 * Handles storing and retrieving Models
 * 
 * @copyright  Copyright (c) 2012 Giacomo Barbalinardo
 * @author     Giacomo Barbalinardo 
 */

class fModel extends fActiveRecord {

	public static $urlPath = '';

	static function findAll()
    {
        return fRecordSet::build(get_called_class());
    }

    /**
     * Creates all models related URLs for the site.
     */
    static public function makeURL($type, $obj=NULL)
    {
        switch ($type)
        {
            case 'list':
                return static::$urlPath;
            case 'add':
                return static::$urlPath . '/add';
            case 'edit':
                return static::$urlPath . '/' . $obj->getId() . '/edit';
            case 'delete':
                return static::$urlPath . '/' . $obj->getId() . '/delete';
        }   
    }
}