<?php
    include 'inc/init.php';

    $tmpl->set('title', 'Start');
    $tmpl->set('page_title', 'fFormGenerator - Sample 1');
    $tmpl->add('css', 'media/css/bootstrap.css');
    $tmpl->add('css', 'media/css/main.css');
    //$tmpl->add('js',  'media/js/init.js');
    $tmpl->place('header');
?>
<div class="container">
<?php
    $form = new fForm(array('action' => fUrl::get(), 'method' => 'POST'));
    $form->addElement(new fFormLabel(array('name' => 'name', 'value' => 'Name eingeben', 'cssClass' => 'control-label')))
         ->addElement(new fFormInput(array('name' => 'name', 'type' => 'text', 'value' => 'foobar', 'cssClass' => 'foo')))
         ->addElement(new fFormTextarea(array('name' => 'cr')))
         ->addElement(new fFormSelect(array('name' => 'selector', 'values' => array(1 => 'test1', 2 => 'test2'), 'value'=> '2')))
         ->addElement(new fFormInput(array('name' => 'submit', 'type' => 'submit', 'value' => 'Send', 'cssClass' => 'btn btn-primary')))
         ->addElement(new fFormCheck(array('name' => 'zutat[]', 'values' => array('pizza'), 'value' => 0)))
         ->addElement(new fFormCheck(array('name' => 'zutat[]', 'values' => array('pilz'), 'value' => 0)))
         ->addElement(new fFormInput(array('name' => 'request_token', 'type' => 'hidden', 'value' => fRequest::generateCSRFToken(), 'fixed' => true)));


//    $form = new fForm(array('action' => fUrl::get(), 'method' => 'POST', 'cssClass' => 'form-horizontal'));
//    $controlGroup = new fFormDecorator(array('tag' => 'div', 'cssClass' => 'control-group'));
//    $controlGroup->addElement(new fFormLabel(array('name' => 'first_name', 'value' => 'Frist name', 'cssClass' => 'control-label')));
//    $controls = new fFormDecorator(array('tag' => 'div', 'cssClass' => 'controls'));
//    $controls->addElement(new fFormInput(array('name' => 'first_name', 'type' => 'text', 'cssClass' => 'required')));
//    $controlGroup->addElement($controls);
//
//    $form->addElement($controlGroup)
//         ->addElement(new fFormInput(array('name' => 'submit', 'type' => 'submit', 'value' => 'Send', 'cssClass' => 'btn btn-primary pull-right')));
echo $form;
?>
</div>
<?php
    $tmpl->place('footer');
	// Das ist ein Kommentartest
?>